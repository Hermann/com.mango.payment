package com.mango.payment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.gms.wallet.AutoResolveHelper;
import com.google.android.gms.wallet.CardRequirements;
import com.google.android.gms.wallet.IsReadyToPayRequest;
import com.google.android.gms.wallet.PaymentData;
import com.google.android.gms.wallet.PaymentDataRequest;
import com.google.android.gms.wallet.PaymentMethodTokenizationParameters;
import com.google.android.gms.wallet.PaymentsClient;
import com.google.android.gms.wallet.TransactionInfo;
import com.google.android.gms.wallet.Wallet;
import com.google.android.gms.wallet.WalletConstants;
import com.mango.payment.settings.SettingsActivity;

import java.util.Arrays;

import timber.log.Timber;

public class MainActivity extends AppCompatActivity {
    private static final int LOAD_PAYMENT_DATA_REQUEST_CODE = 0xbeef;
    private PaymentsClient paymentsClient;
    private FloatingActionButton paymentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        paymentsClient = Wallet.getPaymentsClient(this, new Wallet.WalletOptions.Builder().setEnvironment(WalletConstants.ENVIRONMENT_TEST).build());

        checkReadyToPayWithGoogle();

        paymentButton = findViewById(R.id.fab);
        paymentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                launchPayWithGoogle();
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == LOAD_PAYMENT_DATA_REQUEST_CODE) {
            switch (resultCode) {
                case RESULT_OK:
                    PaymentData paymentData = PaymentData.getFromIntent(data);
                    if (paymentData != null) {
                        String token = paymentData.getPaymentMethodToken().getToken();
                        Timber.i("Payment token: %s", token);
                        showSnackBar("Got token: " + token);
                    }
                    break;
                case RESULT_CANCELED:
                    Timber.i("Payment cancelled");
                    break;
                case AutoResolveHelper.RESULT_ERROR:
                    Status statusFromIntent = AutoResolveHelper.getStatusFromIntent(data);
                    Timber.w("Error, status was: %s", statusFromIntent);
                    break;
                default:
                    Timber.i("Unhandled result code: " + resultCode);
            }
        }
    }

    private void checkReadyToPayWithGoogle() {
        IsReadyToPayRequest request = IsReadyToPayRequest.newBuilder()
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .build();

        Task<Boolean> readyToPay = paymentsClient.isReadyToPay(request);

        readyToPay.addOnCompleteListener(new OnCompleteListener<Boolean>() {
            @Override
            public void onComplete(@NonNull Task<Boolean> task) {
                try {
                    boolean readyToPay = task.getResult(ApiException.class);
                    Timber.i("Ready to pay check completed, ready to pay: %s", readyToPay);
                    paymentButton.setVisibility(readyToPay ? View.VISIBLE : View.GONE);
                    if (!readyToPay) {
                        showSnackBar("This device is not ready to pay.");
                    }
                } catch (ApiException e) {
                    Timber.e(e, "Ready to pay check failed");
                }
            }
        });
    }

    private void launchPayWithGoogle() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        if (preferences.contains("gateway") && preferences.contains("gatewayMerchantId")) {
            PaymentMethodTokenizationParameters tokenParameters = PaymentMethodTokenizationParameters.newBuilder().setPaymentMethodTokenizationType(WalletConstants.PAYMENT_METHOD_CARD)
                    .addParameter("gateway", preferences.getString("gateway", "specify gateway in prefs"))
                    .addParameter("gatewayMerchantId", preferences.getString("gatewayMerchantId", "specifiy merchantid in prefs"))
                    .build();
            AutoResolveHelper.resolveTask(paymentsClient.loadPaymentData(createPaymentRequest(tokenParameters)), this, LOAD_PAYMENT_DATA_REQUEST_CODE);
        } else {
            showSnackBar("Please configure payment gateway settings.");
        }
    }

    private PaymentDataRequest createPaymentRequest(PaymentMethodTokenizationParameters tokenizationParameters) {
        return PaymentDataRequest.newBuilder()
                .setTransactionInfo(TransactionInfo.newBuilder().setTotalPriceStatus(WalletConstants.TOTAL_PRICE_STATUS_FINAL).setTotalPrice("10.00").setCurrencyCode("USD").build())
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_CARD)
                .addAllowedPaymentMethod(WalletConstants.PAYMENT_METHOD_TOKENIZED_CARD)
                .setCardRequirements(CardRequirements.newBuilder()
                        .addAllowedCardNetworks(Arrays.asList(WalletConstants.CARD_NETWORK_AMEX, WalletConstants.CARD_NETWORK_DISCOVER, WalletConstants.CARD_NETWORK_VISA, WalletConstants.CARD_NETWORK_MASTERCARD))
                        .build())
                .setPaymentMethodTokenizationParameters(tokenizationParameters)
                .build();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_settings) {
            SettingsActivity.launch(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void showSnackBar(String message) {
        Snackbar.make(paymentButton, message, Snackbar.LENGTH_LONG).show();
    }
}
