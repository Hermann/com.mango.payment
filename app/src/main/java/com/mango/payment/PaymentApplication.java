package com.mango.payment;

import android.app.Application;

import timber.log.Timber;

public class PaymentApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
